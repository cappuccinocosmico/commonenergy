### A Pluto.jl notebook ###
# v0.12.17

using Markdown
using InteractiveUtils

# ╔═╡ 7651527c-4221-11eb-214d-a7f6e4d1fed6
3+3

# ╔═╡ 3c305984-4222-11eb-3060-fbc8fbd7225a
function fibonacci(n::Int64)
	if (n<3)
		return 1
	else
		fk=fibonacci(n÷2)
		fko=fibonacci(n÷2+1)
		if (n % 2 == 0)
			return fk*(2fko-fk)
		else
			return fk^2+fko^2
		end
	end
end

# ╔═╡ 2d141634-4227-11eb-25df-4fe31421e0a2
fibonacci(4000)

# ╔═╡ Cell order:
# ╠═7651527c-4221-11eb-214d-a7f6e4d1fed6
# ╠═3c305984-4222-11eb-3060-fbc8fbd7225a
# ╠═2d141634-4227-11eb-25df-4fe31421e0a2
