[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cappuccinocosmico%2Fcommonenergy/HEAD)

[![forthebadge](https://forthebadge.com/images/badges/does-not-contain-treenuts.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/works-on-my-machine.svg)](https://forthebadge.com)

This repository contains open energy analyses of Colorado's decarbonization efforts.  

In 2019, Colorado committed to a 26% reduction in GHGe reductions by 2025, 50% by 2030, and 90% by 2050.  

These analyses are developed in Jupyter notebooks as reproducible research documents.

https://the-turing-way.netlify.app/welcome

The goal is to both publish these as static documents to a website and allow interactive use of the notebooks.  

The former requirement will use Jupyter-book static site generator, since Pandoc can read ipynb files.  

The latter requirement will use Binder to host the notebooks in an interactive environment.  This is set up at mybinder.org.
